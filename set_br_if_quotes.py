import json
import requests
from datetime import datetime

API_URL = 'https://www.infofundos.com/api'

# with requests.Session() as s:
#     download = s.get(CSV_URL, verify=False)
#
#     decoded_content = download.content.decode('utf-8')
#
#     cr = csv.reader(decoded_content.splitlines(), delimiter=';')
#     my_list = list(cr)
#
# # Remove first row
# values = my_list[1:]
# # Sort by date
# values.sort(key=lambda x:datetime.strptime(x[2], "%d/%m/%Y"))

def setPriceForSecurity(symbol, price, dateint):
  price = 1/price
  security = currencies.getCurrencyByTickerSymbol(symbol)
  if not security:
    return
  if dateint:
      snapshot = security.getSnapshotForDate(dateint)
      if snapshot.getDateInt() == dateint:
          print "Price for %s already defined" % (security)
          return
      security.setSnapshotInt(dateint, price).syncItem()
      security.setUserRate(price)
      security.syncItem()
      print "Successfully set price for %s" % (security)

book = moneydance.getCurrentAccountBook()
currencies = book.getCurrencies()

for currency in currencies:
    if currency.getTickerSymbol().startswith('FI_'):
        cnpj = currency.getTickerSymbol().replace('FI_','')
        print(cnpj)
        with requests.Session() as s:
            download = s.get(API_URL + '/fundos/search',
                verify=False,
                params={ 'cnpj': cnpj}
            )

            decoded_content = download.content.decode('utf-8')
            print(decoded_content)
